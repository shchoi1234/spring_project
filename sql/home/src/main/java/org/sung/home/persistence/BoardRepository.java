package org.sung.home.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.sung.home.domain.Board;

import java.util.Collection;
import java.util.List;

public interface BoardRepository extends CrudRepository<Board, Long>{

    public List<Board> findBoardByTitle(String title);

    public Collection<Board> findByWriter(String writer);

    public Collection<Board> findByWriterContaining(String writer);

    public Collection<Board> findByTitleContainingOrContentContaining (String title, String content);

    public Collection<Board> findByTitleContainingAndBnoGreaterThan (String keyword, Long num);

    public Collection<Board> findByBnoGreaterThanOrderByBnoDesc (Long bno);

    public List<Board> findByBnoGreaterThanOrderByBnoDesc(Long bno, Pageable Paging);

    public Page<Board> findByBnoGreaterThan(Long bno, Pageable paging);

    @Query("SELECT b FROM Board b WHERE b.title like %?1% and b.bno > 0 ORDER BY b.bno desc")
    public List<Board> findByTitle(String title);

    @Query("SELECT b from Board b WHERE b.content like %:content%  and b.bno > 0 order by b.bno desc")
    public List<Board> findByContent(@Param("content") String content);

    @Query("select b.bno, b.title, b.writer, b.regdate"
            + " from Board b where b.title like %?1% and b.bno > 0 order by  b.bno desc ")
    public List<Object[]> findByTitle2(String title);


}
