package org.sung.home;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;


import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import org.sung.home.domain.Board;
import org.sung.home.persistence.BoardRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BoardReposiotryTests {

    @Autowired
    private BoardRepository boardRepo;

    @Test
    public void inspect() {

        Class<?> clz = boardRepo.getClass();

        System.out.println(clz.getName());

        Class<?>[] interfaces = clz.getInterfaces();

        Stream.of(interfaces).forEach(inter -> System.out.println(inter.getName()));

        Class<?> superClasses = clz.getSuperclass();

        System.out.println(superClasses.getName());
    }

    //create
    @Test
    public void testInsert() {

        Board board = new Board();

        board.setTitle("게시물의 제목");
        board.setContent("게시물 내용 넣기...");
        board.setWriter("user00");

        boardRepo.save(board);
    }

    //read
    @Test
    public void testRead() {

        boardRepo.findById(1L).ifPresent((board) -> {
            System.out.println(board);
        });
    }

    //update
    @Test
    public void testupdate() {
        System.out.println("Read First ..............................");
        Board board = boardRepo.findById(2L).get();

        System.out.println("Update Title ............................");
        board.setTitle("수정된 제목입니다");

        System.out.println("Call save() ..............................");
        boardRepo.save(board);
    }

    //delete
    @Test
    public void testDelete() {

        System.out.println("DELETE Entity");

        boardRepo.deleteById(2L);

    }


    //select
    @Test
    public void testByWriter() {
        Collection<Board> results = boardRepo.findByWriter("user00");

        results.forEach(
                board -> System.out.println(board)
        );
    }

    //findtitle
    @Test
    public void testByTitle() {

        boardRepo.findBoardByTitle("제목..1")
                .forEach(board -> System.out.println(board));

    }
    //like 기능(% | %)
    @Test
    public void testByWriterContaning() {

        Collection<Board> results = boardRepo.findByWriterContaining("05");

        results.forEach(
                board -> System.out.println(board)
        );
    }


    // or 기능
    @Test
    public void findByTitleContainingOrContentContaining () {

        Collection<Board> results = boardRepo.findByTitleContainingOrContentContaining("05","채우기");

        results.forEach(
                board -> System.out.println(board)
                );
    }

    // 부등호 표시
    @Test
    public void testByTitleAndBno() {
        Collection<Board> results = boardRepo.findByTitleContainingAndBnoGreaterThan("5", 50L);

        results.forEach(
                board -> System.out.println(board)
        );
    }

    //order by 처리
    @Test
    public void testBnoorderBy() {

        Collection<Board> results =
                boardRepo.findByBnoGreaterThanOrderByBnoDesc(90L);
        results.forEach(board -> System.out.println(board));
    }

    // 페이징
    @Test
    public void testBnoOrderByPaging() {

        Pageable paging = PageRequest.of(0,10);

        Collection<Board> results =
                boardRepo.findByBnoGreaterThanOrderByBnoDesc(0L, paging);

        results.forEach(board -> System.out.println(board));
    }

    // Page<T> 타입
    @Test
    public void testBnoPagingSort() {

        Pageable paging = PageRequest.of(19, 10, Sort.Direction.ASC, "bno");

        Page<Board> result = boardRepo.findByBnoGreaterThan(0L, paging);

        System.out.println("PAGE SIZE: " + result.getSize());
        System.out.println("TOTAL PAGES: " + result.getTotalPages());
        System.out.println("TOTAL COUNT: " + result.getTotalElements());
        System.out.println("NEXT: " + result.nextOrLastPageable());
        System.out.println(" previousPageable: " + result.previousPageable());
        List<Board> list = result.getContent();

        list.forEach(board -> System.out.println(board));
    }

    @Test
    public void testByTitle2() {

        boardRepo.findByTitle("17")
                .forEach(board -> System.out.println(board));
    }

    @Test
    public void testByTitle17() {

        boardRepo.findByTitle2("17")
                .forEach(arr -> System.out.println(Arrays.toString(arr)));
    }






}