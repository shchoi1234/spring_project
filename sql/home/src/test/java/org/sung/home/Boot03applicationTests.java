package org.sung.home;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.sung.home.domain.Board;
import org.sung.home.persistence.BoardRepository;

import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Boot03applicationTests {

    @Autowired
    private BoardRepository repo;

    @Test
    public void testInsert200() {

        for (int i = 0; i < 200; i++) {

            Board board = new Board();

            board.setTitle("제목.." + i);
            board.setContent("내용 ...." + i + " 채우기");
            board.setWriter("user0" + (i % 10));
            repo.save(board);

        }

    }


}