package com.sung.choi.controller;

import com.sung.choi.domain.SampleVO;
import javassist.tools.rmi.Sample;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

    @GetMapping("/hello")
    public String sayHello() {

        return "Hello World";
    }

    @GetMapping("/sample")
    public SampleVO makeSample() {
        SampleVO vo = new SampleVO();

        vo.setVal1("v1");
        vo.setVal1("v2");
        vo.setVal1("v3");

        return vo;
    }
}
